import UIKit
import Flutter
import Monetizer


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      
    let controller : FlutterViewController = window?.rootViewController as!
      FlutterViewController
      let channel = FlutterMethodChannel(name: "ro-fe.com/native-code-example", binaryMessenger: controller.binaryMessenger)
    
      channel.setMethodCallHandler({
          [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
          guard call.method == "getMessageFromNativeCode" else {
              result(FlutterMethodNotImplemented)
              return
          }
          self?.getMessage(result: result)
      })
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    private func getMessage(result: FlutterResult) {
        var mon = MonetizerTask(publisher: "testPub")
        mon.start(consentState: MonetizerTask.ConsentState.PENDING,consentListener : {
          state in
            NSLog("------- State changed to \(state)")
        });
        let message = "Message from Swift code"
        if message.isEmpty {
            result(FlutterError(code: "UNAVAILABLE",
                                message: "Message from Swift code is empty",
                                details: nil))
        } else{
            result(message)
        }
    }
}
