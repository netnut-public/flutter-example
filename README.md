# Demo Project

# Demo project for NetNut Framework.

# Steps to install NetNut Framework on IOS.
1.  Open ios/Runner.xcworkspace in Xcode.
2.  Copy given Monetizer.framework file into the Runner folder as in the example.
3.  Click on the most to folder Runner, make sure your on TARGETS -> Runner & General.
4.  Go to Frameworks, Libraries, and Embedded Content and switch Embedded to Embed & Sign.
5.  Click in the same section + and add the following packages: Atomics, NIO, NIOCore, NIOTLS.

# Connecting Flutter to IOS.
1.  Create a channel as shown in the example with some name that will be in both sides.
2.  Create in IOS method-name like in the example 'getMessageFromNativeCode' and call it in dart code by invoking it.
3.  Create a private func youFunc that will contain all logic and will be fired on dart code.
